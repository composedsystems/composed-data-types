## [0.0.8] - 2020-05-22
- Fixed bugs unflattening 1D Types.

## [0.0.7] - 2020-05-10
- Added data types for 1D Assert and 1D Path.

## [0.0.6] - 2020-05-08
- Re-linking fun.

## [0.0.5] - 2020-05-08
- Mass compiled.

## [0.0.4] - 2020-05-08
- Cleaned up the parent implementation and refactored the XYZ to Variant method.

## [0.0.3] - 2020-05-05
- Updated to depend on the Run-Time Assertion package so that we could include an Assert type.

## [0.0.2] - 2020-05-03
- Updated "To Variant" implementation

## [0.0.1] - 2020-05-02
- Initial release
- No readme in place yet
- No testing in place